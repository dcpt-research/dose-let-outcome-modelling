from pyrt_lib_rasmusklitgaard.patient import Patient, Cohort
import numpy as np
import matplotlib.pyplot as plt
import pickle
from load_patients import load_patients
from calculate_vh import calculate_vh
from scipy.ndimage import zoom

print("Starting with loading the patients")
patient_dir = "/mnt/d/simulation_data_dicom"
cohort = load_patients(patient_dir)

print("Cohort has N={} entries".format(cohort.n))

organs = ["Rectum", "Bladder"]
gradient_volume_histograms = []

toxs = ["Grade2andabove", "Grade3.GU.CTCv3"]

nsteps = 400
dosegrid = []

for patient in cohort.list_of_patients:
    dosegrid = patient.rtdose_dose.pixel_array * patient.rtdose_dose.DoseGridScaling

    dx, dy = patient.rtdose_dose.PixelSpacing
    dz = patient.rtdose_dose.GridFrameOffsetVector[1] - patient.rtdose_dose.GridFrameOffsetVector[0]
    dosegrid = zoom(dosegrid, (dz/dx, dy/dx, 1)) # in units of Gy
    

    gx,gy,gz = np.gradient(dosegrid) # in units of Gy / (dx mm)
    gradients = np.sqrt(gx**2 + gy**2 + gz**2) * dx # Now units of Gy/mm

    gradients_in_organs = [gradients[patient.structure_indices[patient.actual_structure_names[organ]]] for organ in organs]

    vhs_in_organs = [calculate_vh(gradient, nsteps=nsteps) for gradient in gradients_in_organs]
    gradient_volume_histograms.append(vhs_in_organs)

for i, organ in enumerate(organs):
    plt.figure()
    for j,grad_vhs in enumerate(gradient_volume_histograms):

        patient = cohort.list_of_patients[j]
        tox = patient.metadata["Grade2andabove"]
        color="blue"
        if tox == 1:
            color = "red"

        x_array, this_organ_vh = grad_vhs[i]
        plt.plot(x_array, this_organ_vh, color=color, alpha=0.3)
    plt.xlabel("Maximum dose gradient [Gy/mm]")
    plt.ylabel("Relative volume")
    plt.title("Dose gradient volume histogram for {}".format(organ))

    plt.savefig("dose_gradient_volume_histogram_{}.png".format(organ))



