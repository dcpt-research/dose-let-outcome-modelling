import matplotlib.pyplot as plt
from extract_data import extract_data
import numpy as np

def plot_dvh_bands(dvhs, labels, colors, title, xlabel, xlim = None, minimum_y_percent = 2, relative_volume = True):
    dvhs0, dvhs1 = dvhs
    label0, label1 = labels
    color0, color1 = colors
    maxdose = 120
    plt.figure()

    dose_range = dvhs0[0][:,0]

    for i, dvh in enumerate(dvhs0):
        if np.prod(dvh.shape) == 0:
            dvh = np.array([dose_range,dose_range*0]).transpose()
            dvhs0[i] = dvh
        plt.plot(dose_range, dvh[:,1], color = color0, alpha = 0.05)
    for i, dvh in enumerate(dvhs1):
        if np.prod(dvh.shape) == 0:
            dvh = np.array([dose_range,dose_range*0]).transpose()
            dvhs1[i] = dvh
        plt.plot(dose_range, dvh[:,1], color = color1, alpha = 0.05)
    
    dvhs0_min = np.min(np.array(dvhs0)[:,:,1], axis=0)
    dvhs0_max = np.max(np.array(dvhs0)[:,:,1], axis=0)
    dvhs1_min = np.min(np.array(dvhs1)[:,:,1], axis=0)
    dvhs1_max = np.max(np.array(dvhs1)[:,:,1], axis=0)

    plt.plot(dose_range, dvhs0_min, color = color0, label=label0 + " (n={})".format(len(dvhs0)))
    plt.plot(dose_range, dvhs0_max, color = color0)
    plt.plot(dose_range, dvhs1_min, color = color1, label=label1 + " (n={})".format(len(dvhs1)))
    plt.plot(dose_range, dvhs1_max, color = color1)
    plt.fill_between(dose_range, dvhs0_min, dvhs0_max, color = color0, alpha = 0.3)
    plt.fill_between(dose_range, dvhs1_min, dvhs1_max, color = color1, alpha = 0.3)


    plt.xlabel(xlabel)
    plt.ylabel("Volume [cm^3]")
    if relative_volume:
        plt.ylabel("Volume [%]")
    plt.title(title)
    plt.legend()
    if xlim:
        plt.xlim(xlim)
    else:
        max_xlim_dose0 = dose_range[np.argmin(np.abs(dvhs0_max - minimum_y_percent))]
        max_xlim_dose1 = dose_range[np.argmin(np.abs(dvhs1_max - minimum_y_percent))]
        max_xlim_dose = np.max([max_xlim_dose0, max_xlim_dose1])
        plt.xlim([0,max_xlim_dose + 0.02 * max_xlim_dose])

    savetitle = title.replace(" ","_").replace("/","per").replace("+","p") + ".png"
    savefolder_png = "figures_absolute_png/"
    savefolder_svg = "figures_absolute_svg/"
    if relative_volume:
        savefolder_png = "figures_relative_png/"
        savefolder_svg = "figures_relative_svg/"
    plt.savefig(savefolder_png+savetitle)
    plt.savefig(savefolder_svg+savetitle.replace(".png",".svg"))



if __name__ == "__main__":
    patient_dir = "/mnt/d/simulation_data_dicom"
    # patient_dir = "/mnt/d/testing_cohort"
    ignore_pickles = False
    relative_volume = False
    dvh_dict = extract_data(patient_dir, ignore_pickles = ignore_pickles, relative_volume = relative_volume)
    # dvh_dict : endpoint : outcome : organ : quantity : cut-off -> quantity-VH
    


    plot_labels_dict = {"Grade2andabove" : "Grade 2+ GI", "Grade3.GU.CTCv3" : "Grade 3 GU",  "Grade3.GI.CTCv3" : "Grade 3 GI"}

    for endp, d in dvh_dict.items():
        organs = [list(a.keys()) for a in list(d.values())][0]
        for organ in organs:
            quantities = list(list(d.values())[0][organ].keys())
            for quantity in quantities:
                cut_offs = list(list(d.values())[0][organ][quantity].keys())
                for cut_off in cut_offs:
                    labels = list(d.keys())
                    label0, label1 = labels
                    label0s = d[label0][organ][quantity][cut_off]
                    label1s = d[label1][organ][quantity][cut_off]
                    dvhs = [label0s, label1s]

                    plot_labels = ["With {}".format(plot_labels_dict[endp]), "Without {}".format(plot_labels_dict[endp])]
                    colors = ["red", "blue"]
                    
                    title = "{} - {} VH for {}".format(plot_labels_dict[endp], quantity.capitalize(), organ.lower())
                    xlim = [0,6]
                    units = " [keV/um]"
                    if quantity == "dose":
                        title = title + " for LET above {} keV/um".format(cut_off)
                        xlim = [0,90]
                        units = " [Gy]"
                    if quantity == "let":
                        title = title + " for dose above {} Gy".format(cut_off)
                    xlabel = quantity.capitalize() + units
                    plot_dvh_bands(dvhs, plot_labels, colors, title, xlabel, relative_volume = relative_volume)#, xlim = xlim)


    pass
