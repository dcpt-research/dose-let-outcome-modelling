# Goal is to load in the patients
# Write all DVH, LVH, DLVH and clinical parameters to either
# a big excel sheet, CSV file og some database object
# Probably a CSV file with a header makes the most sense
from load_patients import load_patients
from pyrt_lib_rasmusklitgaard.patient import Patient, Cohort
import numpy as np
from parameter_parser import parameter_parser
import time
from multiprocessing.dummy import Pool as ThreadPool
import itertools

# cohort_dir = "/mnt/d/simulation_data_dicom/"

# print("Loading a single patient first")
# pp = "/mnt/d/testing_cohort/624"

# print("First with bladder wall and rectal wall")
# t0 = time.time()
# p = Patient(pp, "default")
# t1 = time.time()
# print("Loaded with wall structures single patient in {:5.2f} s".format(t1-t0))

# exit()

print("Loading patients")
t0=time.time()
# cohort_dir = "/mnt/d/simulation_data_dicom/"
cohort_dir = "/media/rasmus/3tb_backup/simulation_data/patient_data_with_simulation_results/patients_with_simulation_results_converted_to_dicom/uncompressed/testing/"
cohort_dir = "/media/rasmus/3tb_backup/simulation_data/patient_data_with_simulation_results/patients_with_simulation_results_converted_to_dicom/uncompressed/with_gamma_passes/"
pickle_name = "power_calculation_patient_cohort.pkl"
cohort = load_patients(cohort_dir, ignore_pickles=False, pickle_object_name=pickle_name, keep_anticoagulant=True)
# cohort = load_patients(cohort_dir, ignore_pickles=True)
t1 = time.time()
print("Loaded {} patients in {} s".format(cohort.n, t1-t0))

parameter_list = []

# Metadata stuff
parameter_list.append("patient id")
parameter_list.append("age")
parameter_list.append("anti coagulant")
# parameter_list.append("gi_grade2")
# parameter_list.append("gi_grade2A")
# parameter_list.append("gi_grade2B")
# parameter_list.append("gi_grade3")
# parameter_list.append("gi_grade2")
# parameter_list.append("gu_grade3")

# Anatomical stuff
parameter_list.append("CTV volume")
parameter_list.append("prostate volume")
# parameter_list.append("bladder volume")
# parameter_list.append("bladder wall volume")
# parameter_list.append("rectum volume")
# parameter_list.append("rectal wall volume")

# EUD stuff 
# eud_start = 0.01
# eud_end = 0.5
# eud_step = 0.01
# eudrange = np.arange(eud_start, eud_end+eud_step, eud_step)
# for n in eudrange:
#     parameter_list.append("EUD(n={:4.2f}) bladder".format(n))
# for n in eudrange:
#     parameter_list.append("EUD(n={:4.2f}) bladder wall".format(n))
# for n in eudrange:
#     parameter_list.append("EUD(n={:4.2f}) rectum".format(n))
# for n in eudrange:
#     parameter_list.append("EUD(n={:4.2f}) rectal wall".format(n))

# RBE 1.1
parameter_list.append("RBE1.1 D_{:3.1f}% bladder".format(5))
parameter_list.append("RBE1.1 Dose_V_{:4.1f}_Gy rectal wall".format(75))
parameter_list.append("RBE1.1 Dose_V_{:4.1f}_Gy bladder".format(75))
parameter_list.append("RBE1.1 EUD(n=0.04) rectum")
parameter_list.append("RBE1.1 EUD(n=0.04) rectal wall")
parameter_list.append("RBE1.1 EUD(n=0.04) bladder")
parameter_list.append("RBE1.1 EUD(n=0.04) bladder wall")

# unkelbach RBE = 1 + c * LET stuff
c_start = 0
c_end   = 0.2
c_step  = 0.01
crange = np.arange(c_start, c_end + c_step, c_step)
for c in crange:
    parameter_list.append("unkel c={:4.2f} D_{:3.1f}% bladder".format(c, 5))
    parameter_list.append("unkel c={:4.2f} Dose_V_{:4.1f}_Gy rectal wall".format(c, 75))
    parameter_list.append("unkel c={:4.2f} Dose_V_{:4.1f}_Gy bladder".format(c, 75))
    parameter_list.append("unkel c={:4.2f} EUD(n=9.99) rectum".format(c))
    parameter_list.append("unkel c={:4.2f} EUD(n=9.99) rectal wall".format(c))
    parameter_list.append("unkel c={:4.2f} EUD(n=9.99) bladder".format(c))
    parameter_list.append("unkel c={:4.2f} EUD(n=9.99) bladder wall".format(c))
    

# Max dose stuff (D5%)
# max_dose_start = 0.5
# max_dose_end = 5
# max_dose_step = 0.5
# maxdoserange = np.arange(max_dose_start, max_dose_end+max_dose_step, max_dose_step)
# for x in maxdoserange:
#     parameter_list.append("D_{:3.1f}% bladder".format(x))
# for x in maxdoserange:
#     parameter_list.append("D_{:3.1f}% bladder wall".format(x))
# for x in maxdoserange:
#     parameter_list.append("D_{:3.1f}% rectum".format(x))
# for x in maxdoserange:
#     parameter_list.append("D_{:3.1f}% rectal wall".format(x))

# Dose volume stuff (V10Gy [%])
# dose_volume_start = 5
# dose_volume_end = 80
# dose_volume_step = 2.5
# dosevolumerange = np.arange(dose_volume_start, dose_volume_end+dose_volume_step, dose_volume_step)
# for x in dosevolumerange:
#     parameter_list.append("Dose_V_{:4.1f}_Gy bladder".format(x))
# for x in dosevolumerange:
#     parameter_list.append("Dose_V_{:4.1f}_Gy bladder wall".format(x))
# for x in dosevolumerange:
#     parameter_list.append("Dose_V_{:4.1f}_Gy rectum".format(x))
# for x in dosevolumerange:
#     parameter_list.append("Dose_V_{:4.1f}_Gy rectal wall".format(x))

# LET volume stuff
# let_volume_start = 0.0
# let_volume_end = 8
# let_volume_step = 0.2
# letvolumerange = np.arange(let_volume_start, let_volume_end+let_volume_step, let_volume_step)
# for x in letvolumerange:
#     parameter_list.append("LET_V_{:3.1f}_kev_um bladder".format(x))
# for x in letvolumerange:
#     parameter_list.append("LET_V_{:3.1f}_kev_um bladder wall".format(x))
# for x in letvolumerange:
#     parameter_list.append("LET_V_{:3.1f}_kev_um rectum".format(x))
# for x in letvolumerange:
#     parameter_list.append("LET_V_{:3.1f}_kev_um rectal wall".format(x))

# LET & Dose volume stuff
# for x in dosevolumerange:
#     for y in letvolumerange:
#         parameter_list.append("DoseAndLET_V_{:4.1f}_Gy_{:3.1f}_kev_um bladder".format(x,y))
# for x in dosevolumerange:
#     for y in letvolumerange:
#         parameter_list.append("DoseAndLET_V_{:4.1f}_Gy_{:3.1f}_kev_um bladder wall".format(x,y))
# for x in dosevolumerange:
#     for y in letvolumerange:
#         parameter_list.append("DoseAndLET_V_{:4.1f}_Gy_{:3.1f}_kev_um rectum".format(x,y))
# for x in dosevolumerange:
#     for y in letvolumerange:
#         parameter_list.append("DoseAndLET_V_{:4.1f}_Gy_{:3.1f}_kev_um rectal wall".format(x,y))

# Dose times LET stuff
# letdose_volume_start = 0.0
# letdose_volume_end = 250
# letdose_volume_step = 1
# letdosevolumerange = np.arange(letdose_volume_start, letdose_volume_end+letdose_volume_step, letdose_volume_step)
# for x in letdosevolumerange:
#     parameter_list.append("DoseTimesLET_V_{:5.1f}_Gy_kev_um bladder".format(x))
# for x in letdosevolumerange:
#     parameter_list.append("DoseTimesLET_V_{:5.1f}_Gy_kev_um bladder wall".format(x))
# for x in letdosevolumerange:
#     parameter_list.append("DoseTimesLET_V_{:5.1f}_Gy_kev_um rectum".format(x))
# for x in letdosevolumerange:
#     parameter_list.append("DoseTimesLET_V_{:5.1f}_Gy_kev_um rectal wall".format(x))


# print("In total we have {} parameters per patient".format(len(parameter_list)))
parameter_values_list = []
# parameter_times = {}
# for param in parameter_list:
#         parameter_times[param[:2]] = 0

parameter_values_list = [[0 for i in range(len(parameter_list))] for j in range(len(cohort.list_of_patients))]

def calculate_parameter_ij(target, ij):
    i,j = ij
    target[i][j] = parameter_parser(cohort.list_of_patients[i], parameter_list[j])

n_threads = 15
pool = ThreadPool(n_threads)
list_of_patient_parameter_indices = [(i,j) for i in range(len(cohort.list_of_patients)) for j in range(len(parameter_list))]


print("Calculating parameters per patient in {} threads".format(n_threads))
t00 = time.time()
results = pool.starmap(calculate_parameter_ij, zip(itertools.repeat(parameter_values_list), list_of_patient_parameter_indices))
t11 = time.time()

# for i,patient in enumerate(cohort.list_of_patients):
#     patient_values = []
#     for j,param in enumerate(parameter_list):
#         # t0 = time.time()
#         value = parameter_parser(patient,param)
#         # t1 = time.time()
#         # parameter_times[param[:2]] += t1-t0
#         patient_values.append(value)
#         # print("{:8.2f}".format(value),end=",")
#     parameter_values_list.append(patient_values)
#     # print("\n",end="")  
print("In total {:5.2f} s per patient used in computing needed values".format((t11-t00)/cohort.n))

# for key,value in parameter_times.items():
#     print("Time per calculation spent on parameter type: {} = {}".format(key,value/cohort.n/len([a for a in parameter_list if a[:2] == key])))

output_file_name = "ufhpti_power_calculation_database.csv"
print("Writing output to {}".format(output_file_name))
with open(output_file_name, "w") as f:
        header = ""
        for param_name in parameter_list:
            header = header + param_name + ","
        header = header[:-1]
        f.write(header + "\n")
        for patient_values in parameter_values_list:
            line = ""
            for i, param in enumerate(patient_values):
                try:
                    line = line + "{:8.2f},".format(param)
                except ValueError:
                    print("jello")
                    raise ValueError("Oopsie")
                except TypeError:
                    print("param is: {}".format(parameter_list[i]))
                    print("value is: {}".format(param))
                    print("patient id is: {}".format(patient_values[0]))
                    raise TypeError("We ran into a type problem")
            line = line[:-1]
            f.write(line + "\n") 

