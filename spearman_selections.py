import sklearn.preprocessing as skpp
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.inspection import permutation_importance
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.cluster import hierarchy
from scipy.spatial.distance import squareform
from scipy.stats import spearmanr

print("Imports completed")



datafile_name = "ufhpti_cohort_database.csv"
header = np.genfromtxt(datafile_name,delimiter=",",dtype=str)[0,:]
data = np.genfromtxt(datafile_name, delimiter=",",skip_header=1)
print("CSV data has been loaded")

outcome_columns = [3,4,5,6,7,8] # starts at index 3, ends at index 8
patient_id_column = [0]
skip_columns = patient_id_column + outcome_columns

x_header = [x for i,x in enumerate(header) if i not in skip_columns]
x_data = np.array([column for i,column in enumerate(data.T) if i not in skip_columns]).T

y_header = [y for i,y in enumerate(header) if i in outcome_columns]
y_data = np.array([column for i,column in enumerate(data.T) if i in outcome_columns]).T

# x_data = skpp.normalize(x_data, norm="l2", axis=0) # axis 0 because features are rows


### Example from scikit learn
### Example from https://scikit-learn.org/stable/auto_examples/inspection/plot_permutation_importance_multicollinear.html#sphx-glr-auto-examples-inspection-plot-permutation-importance-multicollinear-py
print("x_data has shape: {}".format(x_data.shape))

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 8))
corr = spearmanr(x_data).correlation


# Ensure the correlation matrix is symmetric
corr = (corr + corr.T) / 2
np.fill_diagonal(corr, 1)

# We convert the correlation matrix to a distance matrix before performing
# hierarchical clustering using Ward's linkage.
distance_matrix = 1 - np.abs(corr)
dist_linkage = hierarchy.ward(squareform(distance_matrix))
dendro = hierarchy.dendrogram(
    dist_linkage, labels=x_data.columns.to_list(), ax=ax1, leaf_rotation=90
)
dendro_idx = np.arange(0, len(dendro["ivl"]))

ax2.imshow(corr[dendro["leaves"], :][:, dendro["leaves"]])
ax2.set_xticks(dendro_idx)
ax2.set_yticks(dendro_idx)
ax2.set_xticklabels(dendro["ivl"], rotation="vertical")
ax2.set_yticklabels(dendro["ivl"])
_ = fig.tight_layout()
plt.show()