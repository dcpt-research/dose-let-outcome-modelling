from pyrt_lib_rasmusklitgaard.patient import Cohort, Patient
from load_patients import load_patients
import pickle
import os
import time, datetime
import random

def extract_data(patient_dir, ignore_pickles = False, relative_volume = True):
    patient_dir = "/mnt/d/simulation_data_dicom"
    # patient_dir = "/mnt/d/testing_cohort"

    pickle_object_name = "loaded_patient_cohort.pkl"
    dvh_pickle_name = "dvh_dictionary_absolute.pkl"
    if relative_volume:
        dvh_pickle_name = "dvh_dictionary_relative.pkl"


    print("Checking if the dvhs have already been pickled.")
    if not ignore_pickles and dvh_pickle_name in os.listdir("."):
        print("Loading dvh dictionary pickle")
        with open(dvh_pickle_name, "rb") as f:
            dvh_dict = pickle.load(f)
        return dvh_dict

    
    cohort = load_patients(patient_dir)
        





    # LET VH for all patients
    endpoints = ["Grade2andabove", "Grade3.GU.CTCv3"]
    endpoints = ["Grade2andabove", "Grade3.GU.CTCv3"]
    outcomes = ["adverse", "control"]
    organs = ["Bladder", "Rectum", "Bladder Wall", "Rectal Wall"]
    quantities = ["dose", "let"]
    dvh_let_values = [0, 1, 2, 2.5, 3]
    lvh_dose_values = [0,5,20,30,40,50,60,70]

    cohort_dictionary = {end_p : {"adverse": cohort.make_subcohort_meta(["{}".format(end_p),"==","1"]), "control": cohort.make_subcohort_meta(["{}".format(end_p),"==","0"])} for end_p in endpoints}

    # dvh_dict = {end_p : {"adverse" : {org : {"dose" : {}, "let" : {}} for org in organs}, "control" : {org : {"dose" : {}, "let" : {}} for org in organs}} for end_p in endpoints}

    dvh_dict = {end_p : {outc : {org : {q : {} for q in quantities} for org in organs} for outc in outcomes} for end_p in endpoints}

    # dvh_dict : endpoint : outcome : organ : quantity : cut-off -> quantity-VH

    number_of_dvh_calculations_per_patient = len(endpoints) * len(outcomes) * len(organs) * len(quantities) * (len(dvh_let_values) +1)
    t00 = time.time()
    random.choice(cohort.list_of_patients).dvh_above_let_value(0, random.choice(organs))
    t11 = time.time()
    total_estimated_time = (t11-t00) * cohort.n * number_of_dvh_calculations_per_patient
    print("There will be {} dvh calculations for all {} patients".format(number_of_dvh_calculations_per_patient, cohort.n))
    print("With an estimated time of {} s per dvh".format(t11-t00))
    print("Estimated total time: {}".format(str(datetime.timedelta(seconds=total_estimated_time))))



    print("Calculating all the dvhs")
    for e, adv_control_dict in cohort_dictionary.items():
        for label, c in adv_control_dict.items():
            for org in organs:
                print("Calulating dvhs for endpoint: {} - {} in {}".format(e,label,org))
                # initiatlizing lists of dvhs
                dvh_dict[e][label][org]["let"][0] = []
                for let_value in dvh_let_values:
                    dvh_dict[e][label][org]["dose"][let_value] = []
                # Calculating dvhs per patient
                for dose_value in lvh_dose_values:
                    dvh_dict[e][label][org]["let"][dose_value] = []
                for patient in c.list_of_patients:
                    for let_value in dvh_let_values:
                        dvh_dict[e][label][org]["dose"][let_value].append(patient.dvh_above_let_value(let_value, org, relative_volume = relative_volume))
                    for dose_value in lvh_dose_values:
                        dvh_dict[e][label][org]["let"][dose_value].append(patient.dvh_above_let_value(dose_value, org, dose_is_let=True, relative_volume = relative_volume))

    print("Saving all dvhs in a pickle object.")
    with open(dvh_pickle_name, "wb") as f:
        if not ignore_pickles:
            pickle.dump(dvh_dict, f)
        else:
            pass

    return dvh_dict


if __name__ == "__main__":
    patient_dir = "/mnt/d/simulation_data_dicom"
    dvh_dictionary = extract_data(patient_dir)
# for patient 
# letgrid = patient.rtdose_letd.pixel_array * patient.rtdose_letd.DoseGridScaling
# letd_grids_in_orgs = [letdgrid[patient.structure_indices[orgs]] for orgs in organs]
# calculate_vh()

# DVH for all patients

# RBE weighed VH for all patients