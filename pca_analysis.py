import sklearn.preprocessing as skpp
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.inspection import permutation_importance
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import spearmanr
from time import sleep
print("Imports completed")

datafile_name = "ufhpti_cohort_database.csv"
header = np.genfromtxt(datafile_name,delimiter=",",dtype=str)[0,:]
data = np.genfromtxt(datafile_name, delimiter=",",skip_header=1)
print("CSV data has been loaded")

outcome_columns = [3,4,5,6,7,8] # starts at index 3, ends at index 8
patient_id_column = [0]
skip_columns = patient_id_column + outcome_columns

x_header = [x for i,x in enumerate(header) if i not in skip_columns]
x_data = np.array([column for i,column in enumerate(data.T) if i not in skip_columns]).T

y_header = [y for i,y in enumerate(header) if i in outcome_columns]
y_data = np.array([column for i,column in enumerate(data.T) if i in outcome_columns]).T

x_data = skpp.normalize(x_data, norm="l2", axis=0) # axis 0 because features are rows

pca = PCA()
pca.fit(x_data)

x_pca = pca.transform(x_data)
pca_full_data = np.append(y_data, x_pca, axis = 1)


pca_correlation_matrix = spearmanr(pca_full_data).correlation

for i in range(y_data.shape[1]):
    c0, c1 = np.argpartition(np.abs(pca_correlation_matrix[y_data.shape[1]:,i]),-2)[-2:]
    c0 = c0 + y_data.shape[1]
    c1 = c1 + y_data.shape[1]
    ys = y_data[:,i]
    colors = ["red", "blue"]
    labels = [y_header[i], "Not {}".format(y_header[i])]
    values = [1, 0]
    plt.figure()
    for color, jj, target_name in zip(colors, values, labels):
        plt.plot(pca_full_data[ys == jj, c0], pca_full_data[ys == jj, c1], '.', alpha = 0.7, color=color, label = target_name)
        plt.title("Most endpoint explaining PCA components for {}".format(y_header[i]))
        plt.legend()
        plt.xlabel("PCA component {}".format(c0))
        plt.ylabel("PCA component {}".format(c1))
        plt.savefig("pca_best_explaining_{}.png".format(y_header[i]))


spearman_corrs = spearmanr(data).correlation
spearman_corrs[np.where(np.isnan(spearman_corrs))] = 1
spearman_corrs = np.abs(spearman_corrs)
# index 15 actual data starts

print("Starting loop")
i=15

[i.split("_")[0].split("(")[0] for i in header]
header_start = header[i].split("_")[0].split("(")[0]
species_of_data = []
data_species_index_edges = []

keep_these_indices_after_spearman_coeffs_and_variance_ranking = []
organ_types = ["rectum", "rectal wall", "bladder", "bladder wall"] 
#  important to have bladder wall last, since "bladder" is in "bladder wall" (string)
while i<len(header):
    for ii in range(i,len(header)):
        if header[ii].split("_")[0].split("(")[0] != header_start:
            species_of_data.append(header_start)
            data_species_index_edges.append((i,ii-1))
            print("We found species {} in range {}-{}".format(header_start,i,ii-1))
            i = ii
            header_start = header[i].split("_")[0].split("(")[0]
            break
    if header_start == header[-1].split("_")[0].split("(")[0]:
        species_of_data.append(header_start)
        data_species_index_edges.append((i,len(header)-1))
        print("We found species {} in range {}-{}".format(header_start,i,len(header)-1))
        break

for i,species in enumerate(species_of_data):
    start,end = data_species_index_edges[i]
    all_headers_of_this_species = header[start:end+1]
    threshold_corr_coeff = 0.2
    organs_available_in_this_species = set()
    for head in all_headers_of_this_species:
        for org in organ_types:
            if org in head:
                organs_available_in_this_species.add(org)
    organs_available_in_this_species = list(organs_available_in_this_species)
    # The organ parts are equal sized
    n_organs = len(organs_available_in_this_species)
    n_per_organ = int(all_headers_of_this_species.shape[0]/n_organs)
    organ_indices = [(0+i*n_per_organ, (i+1)*n_per_organ-1) for i in range(n_organs)]

    organ_indices_in_original_data_matrix = [(start + orgstart, start + orgend) for orgstart, orgend in organ_indices]

    for i,organ in enumerate(organs_available_in_this_species):
        ind_start, ind_end = organ_indices_in_original_data_matrix[i]
        this_species_organ_spearman = spearman_corrs[ind_start:ind_end, ind_start:ind_end]
        plt.figure()
        plt.imshow(this_species_organ_spearman, cmap="viridis", vmin = 0, vmax = 1)
        plt.colorbar()
        plt.title("species: {}, organ: {}".format(species, organ))
plt.show(block=False)



    # now loop over organs available in this header, and get the indices in the total data matrix where each organ is
    # Then from these indices, we can get the spearman corrs for this species and organ.
    # Then we will do one-by-one elimination of each v_a,v_b with under threshold_corr_coeff
    # So if spearman(v_a, v_b) < 0.2, then
        # eliminate the one with the lowest variance
        # We can do this by writing down the indices we want to keep in a list