import numpy as np
import scipy.stats as sps
import matplotlib.pyplot as plt
# load in the CSV file

filename = "ufhpti_cohort_database.csv"
data = []
with open(filename, "r") as f:
    for i,line in enumerate(f):
        values = line.split(",")
        values = [string.strip() for string in values]
        # If none of the members are not numeric
        # Then we consider them all to be numbers (hopefully)
        if not False in [string.replace(".","").isnumeric() for string in values]:
            values = [float(value) for value in values]
        else:
            for value in values:
                if not value.isnumeric() and i != 0:
                    print("This value is not numeric: {}".format(value))
        # Otherwise keep them as is
        data.append(values)
print("Data loaded succesfully. {} Lines loaded.".format(len(data)-1))
header = data[0]
data = data[1:]
data = np.array(data)

data = data[:,9:]

# normalize data using a standard score ( x-mu ) / sigma
means = np.mean(data,axis=0)
stds  = np.std(data,axis=0)

normalized_data = (data - means) / stds

i,j = np.random.randint(normalized_data.shape[0]),np.random.randint(normalized_data.shape[1])
print("The type of randomly selected entry ({},{}) is: {}".format(i,j,type(normalized_data[i,j])))

print("The shape of the total data matrix is: {}".format(normalized_data.shape))

# Do spearman correlation coefficient stuff for some data reduction
if np.isnan(normalized_data).any():
    print("There are 'nan's in the data!")

stats, ps = sps.spearmanr(normalized_data)

print("There are {} nans in the statistic object".format(stats[np.where(np.isnan(stats)==True)].shape[0]))

stats[np.where(np.isnan(stats)==True)] = 1


plt.figure()
plt.imshow(np.abs(stats),cmap="viridis")
plt.colorbar()
plt.savefig("ludvig_color_coded_figure.png")

