import scipy.ndimage as ndimage
import numpy as np
from load_patients import load_patients
from pyrt_lib_rasmusklitgaard.patient import Cohort,Patient
import matplotlib.pyplot as plt

def calculate_positive_volume(array):
    # Step 1: Identify strictly positive regions
    positive_regions = array > 0

    # Step 2: Label connected components in the positive regions
    labeled_regions, num_labels = ndimage.label(positive_regions)

    # Step 3: Calculate the volume of each identified region
    volumes = ndimage.sum(positive_regions, labeled_regions, range(1, num_labels + 1))

    # Step 4: Choose the largest volume of all regions
    if np.prod(np.shape(volumes)) == 0:
        return [0]

    return volumes


patient_dir = "/mnt/d/simulation_data_dicom"
cohort = load_patients(patient_dir)

print("Cohort has N={} entries".format(cohort.n))

organs = ["Rectum", "Bladder"]
gradient_volume_histograms = []

toxs = ["Grade2andabove", "Grade3.GU.CTCv3"]
plot_labels_dict = {"Grade2andabove" : "Grade 2+ GI", "Grade3.GU.CTCv3" : "Grade 3 GU",  "Grade3.GI.CTCv3" : "Grade 3 GI"}
organ_dict = {"Grade2andabove": "Rectum", "Grade3.GU.CTCv3": "Bladder"}

patients_with_without = [(cohort.make_subcohort_meta(["{}".format(tox),"==","1"]), cohort.make_subcohort_meta(["{}".format(tox),"==","0"])) for tox in toxs]
for i, tox in enumerate(toxs):
    print("Calculating volumes for {}".format(plot_labels_dict[tox]))
    with_tox, without_tox = patients_with_without[i]
    
    organ = organ_dict[tox]
    print("Calculating for tox positive patients")
    
    plt.figure()
    plt.title("Histograms of volumes of connected LET induced hot spot in {}".format(organ_dict[tox]))
    plt.xlabel("Positive volumes [cm^3]")
    plt.ylabel("Density [arb]")
    for patient in with_tox.list_of_patients:
        patient.set_unkelbach_rbe_weighed_dose()
        dosediff = patient.unkelbach_rbe_weighed_dose.pixel_array * patient.unkelbach_rbe_weighed_dose.DoseGridScaling - patient.rtdose_dose.pixel_array * patient.rtdose_dose.DoseGridScaling

        organ_inds = patient.structure_indices[patient.actual_structure_names[organ]]
        filter_arr = np.zeros_like(dosediff)
        filter_arr[organ_inds] = 1
        dosediff = dosediff * filter_arr

        color = "red"

        dx,dy = patient.rtdose_dose.PixelSpacing
        dz = patient.rtdose_dose.GridFrameOffsetVector[1] - patient.rtdose_dose.GridFrameOffsetVector[0]
        V = dx*dy*dz / 1000
        volume_sizes = [v*V for v in calculate_positive_volume(dosediff)]
        plt.hist(volume_sizes, color=color, alpha=0.5)

    print("Calculating for tox negative patients")
    for patient in without_tox.list_of_patients:
        patient.set_unkelbach_rbe_weighed_dose()
        dosediff = patient.unkelbach_rbe_weighed_dose.pixel_array * patient.unkelbach_rbe_weighed_dose.DoseGridScaling - patient.rtdose_dose.pixel_array * patient.rtdose_dose.DoseGridScaling

        organ_inds = patient.structure_indices[patient.actual_structure_names[organ]]
        filter_arr = np.zeros_like(dosediff)
        filter_arr[organ_inds] = 1
        dosediff = dosediff * filter_arr

        color = "blue"

        dx,dy = patient.rtdose_dose.PixelSpacing
        dz = patient.rtdose_dose.GridFrameOffsetVector[1] - patient.rtdose_dose.GridFrameOffsetVector[0]
        V = dx*dy*dz / 1000
        volume_sizes = [v*V for v in calculate_positive_volume(dosediff)]
        plt.hist(volume_sizes, color=color, alpha=0.5)

    plt.savefig("hist_volumes_{}.png".format(organ_dict[tox]))