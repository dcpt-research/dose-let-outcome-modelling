import time
from pyrt_lib_rasmusklitgaard import Patient, Cohort
from pyrt_lib_rasmusklitgaard.lkb_ntcp import calculate_lkb, eud_calculator_dose_array
from load_patients import load_patients
from scipy import stats
import os
import numpy as np
from numpy.polynomial.polynomial import polyfit
import matplotlib.pyplot as plt

### See https://en.wikipedia.org/wiki/Hosmer-Lemeshow_test
print("Loading patients")
t0=time.time()
cohort_dir = "/mnt/d/simulation_data_dicom/"
cohort_dir = "/mnt/d/testing_cohort/"
pickle_name = "loaded_patient_cohort.pkl"
pickle_name = "test_cohort.pkl"
# cohort = load_patients(cohort_dir, ignore_pickles=False, pickle_object_name = pickle_name)
t1 = time.time()
# print("Loaded {} patients in {} s".format(cohort.n, t1-t0))


## Calculate variable RBE for all the patients
def var_rbe_dose(dose, let, alphabeta):
    return dose/1.1 * (1+0.04*let)

## NTCP function parameters
# QUANTEC
TD50 = 76.9 # Gy
n = 0.09
m = 0.13
structure = "Rectum"

Q = 10 # divide into 10 equally sized parts

# for patient in cohort:
#   indices = patient.structure_indices[actual_organ_name]
#   dose_array_in_structure = dose_array[indices]
# 	eud_in_structure = eud_calculator_dose_array(dose_array_in_structure, n)
#	lkb_ntcp = calculate_lkb(eud_in_structure, d50, m)
data_table = [] 
# One line per patient
# Should be [   probability of event / NTCP (given variable RBE)   ;   probability of NOT event (1-NTCP)   ;   observed event (1 yes event, 0 no event)   ;   NOT observed event   (0 yes event, 1 no event)]


for patient_fold in os.listdir(cohort_dir):
    print("Calculating variable RBE NTCP for: {:5d}".format(int(patient_fold)))
# for patient in cohort.list_of_patients:
    patient_path = cohort_dir + "/" + patient_fold
    patient = Patient(patient_path)
    actual_structure_name = patient.actual_structure_names[structure]
    structure_indices = patient.structure_indices[actual_structure_name]
    dose_array = patient.rtdose_dose.pixel_array * patient.rtdose_dose.DoseGridScaling
    letd_array = patient.rtdose_letd.pixel_array * patient.rtdose_letd.DoseGridScaling
    variable_rbe_dose_array = var_rbe_dose(dose_array, letd_array, 0)
    variable_rbe_dose_in_structure = variable_rbe_dose_array[structure_indices]
    var_rbe_eud_in_structure = eud_calculator_dose_array(variable_rbe_dose_in_structure, n)
    calculated_ntcp_var_rbe = calculate_lkb(var_rbe_eud_in_structure, TD50, m)
    event = float(patient.metadata["Grade2andabove"])
    this_patient_data = [calculated_ntcp_var_rbe, (1-calculated_ntcp_var_rbe), event, 1-event]
    data_table.append(this_patient_data)

# sort from largest to smallest
sorted_data_table = sorted(data_table,key=lambda x: -x[0])
if len(sorted_data_table) < Q:
    print("Not enough data to divide into {} parts. There must be (at the very very least) {}, there is {}.".format(Q,Q,len(sorted_data_table)))

elements_per_group = int( len(sorted_data_table) / Q )
remainder = len(sorted_data_table) % Q
group_sizes = [elements_per_group for i in range(Q)]
for i in range(remainder):
    group_sizes[i] += 1

grouped_data = [[] for i in range(len(group_sizes))]
target_group = 0
for datapoint in sorted_data_table:
    if len(grouped_data[target_group]) >= group_sizes[target_group]:
        target_group += 1
    grouped_data[target_group].append(datapoint)

print(group_sizes)
            

Hosmer_lemeshow_test_size = 0
for grp in reversed(grouped_data):
    probyes = [l[0] for l in grp]
    probnot = [l[1] for l in grp]
    obsyes  = sum([int(l[2]) for l in grp])
    obsnot  = sum([int(l[3]) for l in grp])
    expyes  = sum(probyes)
    expnot  = sum(probnot)

    contribution = (obsyes - expyes)**2 / expyes + (obsnot - expnot)**2 / expnot
    Hosmer_lemeshow_test_size = Hosmer_lemeshow_test_size + contribution


    print("| [{:5.3f};{:5.3f}] | {:2d} | {:2d} | {:5.2f} | {:5.2f} |".format(min(probyes),max(probyes),obsnot,obsyes,expnot,expyes))

p_value = 1 - stats.chi2.cdf(Hosmer_lemeshow_test_size, Q-2)
print("p value {:5.3f}".format(p_value))

occurence_mins = []
occurence_maxs = []
occurence_esti = []
ntcps = []
ntcps_stderr = []
# 95 % confidence interval
z = 1.96
for grp in grouped_data:
    n_yes = sum([int(l[2]) for l in grp])
    n_not = sum([int(l[3]) for l in grp])
    base = ( n_yes + 0.5 * z**2 ) / ( n_yes + n_not + z**2)
    pm = z/(n_yes + n_not + z**2) * ( (n_yes * n_not)/(n_yes + n_not) + z**2 / 4 )**0.5
    occurence_mins.append(base-pm)
    occurence_maxs.append(base+pm)
    occurence_esti.append(n_yes / ( n_yes + n_not ))
    ntcp_values = [l[0] for l in grp]
    ntcps.append(np.mean(ntcp_values))
    ntcps_stderr.append(1.96*np.std(ntcp_values)/len(grp))



occurence_errs = [[y-lower, upper-y] for y, lower, upper in zip(occurence_esti, occurence_mins, occurence_maxs)]

occurence_mins = np.array(occurence_mins) * 100
occurence_maxs = np.array(occurence_maxs) * 100
occurence_esti = np.array(occurence_esti) * 100
occurence_errs = np.array(occurence_errs).T * 100
ntcps = np.array(ntcps) * 100
ntcps_stderr = np.array(ntcps_stderr) * 100


b, a = polyfit(ntcps, occurence_esti,1)


plt.figure()
# dashed line
plt.plot([0,0],[100,100],'--',color="black")
# data points
plt.errorbar(ntcps, occurence_esti, yerr = occurence_errs, xerr = ntcps_stderr, capsize=5, color="black",  ls='none')
# best fit
plt.plot(ntcps, a*ntcps + b, '-', color="black")
# xlims ylims
plt.xlim([0,100])
plt.ylim([0,100])
plt.axis("equal")
plt.savefig("hosmer_lemeshow_calibration_plot.png")





