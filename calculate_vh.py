import numpy as np
def calculate_vh(data_array, stepsize=0.1, nsteps = None):
    data_array = np.array(data_array)
    full_volume = np.prod(np.shape(data_array))

    value_array = np.array([0,0])
    if stepsize != 0:
        value_array = np.arange(0, np.max(data_array), stepsize)

    if nsteps:
        value_array = np.linspace(0, np.max(data_array), nsteps)
    if len(value_array) == 1:
        value_array = np.append(value_array, stepsize)
    volume_array = np.zeros_like(value_array)
    for i,v in enumerate(value_array):
        sub_volume = np.prod(np.shape(data_array[data_array>v]))
        volume_array[i] = sub_volume
    return (value_array, volume_array / full_volume)
