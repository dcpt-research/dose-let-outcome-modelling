import numpy as np
import scipy.stats as sps
import matplotlib.pyplot as plt
from scipy.stats import spearmanr
# load in the CSV file
def homemade(filename):
    data = []
    with open(filename, "r") as f:
        for i,line in enumerate(f):
            values = line.split(",")
            values = [string.strip() for string in values]
            # If none of the members are not numeric
            # Then we consider them all to be numbers (hopefully)
            if not False in [string.replace(".","").isnumeric() for string in values]:
                values = [float(value) for value in values]
            else:
                for value in values:
                    if not value.isnumeric() and i != 0:
                        print("This value is not numeric: {}".format(value))
            # Otherwise keep them as is
            data.append(values)
    print("Data loaded succesfully. {} Lines loaded.".format(len(data)-1))
    header = np.array(data[0])
    data = np.array(data[1:])
    return header, data

def numpy_solution(datafile_name):
        
    datafile_name = "ufhpti_cohort_database.csv"
    header = np.genfromtxt(datafile_name,delimiter=",",dtype=str)[0,:]
    data = np.genfromtxt(datafile_name, delimiter=",",skip_header=1)
    print("CSV data has been loaded")
    return header, data



filename = "ufhpti_cohort_database.csv"
h0,d0 = homemade(filename)
h1,d1 = numpy_solution(filename)

outcome_columns = [3,4,5,6,7,8] # starts at index 3, ends at index 8
patient_id_column = [0]
skip_columns = patient_id_column + outcome_columns


xh0 = [x for i,x in enumerate(h0) if i not in skip_columns]
xd0 = np.array([column for i,column in enumerate(d0.T) if i not in skip_columns]).T

yh0 = [y for i,y in enumerate(h0) if i in outcome_columns]
yd0 = np.array([column for i,column in enumerate(d0.T) if i in outcome_columns]).T

xh1 = [x for i,x in enumerate(h1) if i not in skip_columns]
xd1 = np.array([column for i,column in enumerate(d1.T) if i not in skip_columns]).T

yh1 = [y for i,y in enumerate(h1) if i in outcome_columns]
yd1 = np.array([column for i,column in enumerate(d1.T) if i in outcome_columns]).T


td0 = np.array([column for i,column in enumerate(d1.T) if i in [j-10 for j in range(len(d1.T)+100)]]).T

a = np.random.rand(8,20)
a[:,18] = 0
a[:,17] = 1

ta = np.delete(d0, skip_columns[0],axis=1)
for i in skip_columns[1:]:
    ta = np.delete(ta, i, axis=1)
