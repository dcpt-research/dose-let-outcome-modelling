import numpy as np
import matplotlib.pyplot as plt
datafile_name = "ufhpti_power_calculation_database.csv"
header = np.genfromtxt(datafile_name,delimiter=",",dtype=str)[0,:]
data = np.genfromtxt(datafile_name, delimiter=",",skip_header=1)

col = data[:,7]
plt.hist(col)
plt.show()