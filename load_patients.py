from pyrt_lib_rasmusklitgaard.patient import Patient, Cohort
from pyrt_lib_rasmusklitgaard.helpers import get_patient_metadata
import openpyxl
import time
import datetime
import os
import pickle

def load_patients(cohort_dir, ignore_pickles = False, pickle_object_name = "loaded_patient_cohort.pkl", keep_anticoagulant = False):

    # pickle_object_name = "loaded_patient_cohort.pkl"
    if not ignore_pickles:
        print("Checking if we already have a pickled cohort")
        if pickle_object_name in os.listdir("."):
            print("Loading pickled cohort ...")
            t0 = time.time()
            with open(pickle_object_name, "rb") as f:
                cohort = pickle.load(f)
            print("Pickle object loaded in {}".format(str(datetime.timedelta(seconds=time.time() - t0))))
            return cohort
    t0 = time.time()
    path_to_main_patient_data_xlsx = "/media/rasmus/3tb_backup/ufhpti_all_data/main_patient_data_numerical.xlsx"
    xlsx_workbook = openpyxl.load_workbook(filename=path_to_main_patient_data_xlsx, data_only=True)
    patient_data_sheet = xlsx_workbook["Sheet1"]
    print("Loaded workbook in {:8.1f} s".format(time.time()-t0))

    number_of_patients_antic=0
    number_of_patients = 0
    

    cohort_no_antic = Cohort()

    file_list = os.listdir(cohort_dir)
    number_of_reads = len([d for d in file_list if d[-4:] != ".tar"])
    print("Preparing to read {} patients. Expected time assuming ~6 s per read: {}".format(number_of_reads, str(datetime.timedelta(seconds=number_of_reads * 6))))

    i = 1
    print("Reading {:4d} / {:4d}".format(0, number_of_reads), end="")
    for patient_name in os.listdir(cohort_dir):
        print("\r",end="")
        print("Reading {:4d} / {:4d}".format(i, number_of_reads), end="")
        if patient_name[-4:] == ".tar": # skips the ones still archived in a .tar
            continue
        if patient_name[-3:] == ".sh": # skips .sh files
            continue
        if patient_name[-4:] == ".txt": # skips .txt files
            continue
        if patient_name[-4:] == ".swp": # skips .txt files
            continue
        i = i+1
        t0 = time.time()
        number_of_patients += 1
        patient_id = patient_name
        patient_dir = cohort_dir + "/" + patient_name
        this_patient_metadata = get_patient_metadata(patient_data_sheet,patient_id)
        if this_patient_metadata == {}:
            # In case a patient has no metadata in the table, we cant use it.
            continue
            
        if this_patient_metadata["AntiC"] == 1:
            number_of_patients_antic +=1
            if not keep_anticoagulant:
                continue
        # now load patient if not antiC.

        # print("Adding patient directory: {}".format(patient_dir))
        newpat = Patient(patient_dir, wanted_structures="default")#, xlsx_workbook=xlsx_workbook)
        

        if newpat.dict_of_gamma_passes["BODY"][3] < 90:
            continue
        cohort_no_antic.add_patient(newpat)

        # Remove the unpacked files
        t1 = time.time()
        # print("Added a patient in {} s".format(t1-t0))
    print("\n", end="")


    print("Now we have {} entries in cohort".format(cohort_no_antic.n))

    print("In total {} / {} received antiC".format(number_of_patients_antic, number_of_patients))

    if not ignore_pickles:
        print("Cohort loaded, saving to .pkl ...")
        with open(pickle_object_name, "wb") as f:
            pickle.dump(cohort_no_antic, f)    
    return cohort_no_antic
