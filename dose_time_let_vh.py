from pyrt_lib_rasmusklitgaard.patient import Patient, Cohort
import numpy as np
import matplotlib.pyplot as plt
import pickle
from load_patients import load_patients
from calculate_vh import calculate_vh
from scipy.ndimage import zoom

np.seterr(divide='ignore')

print("Starting with loading the patients")
patient_dir = "/mnt/d/simulation_data_dicom"
cohort = load_patients(patient_dir)

print("Cohort has N={} entries".format(cohort.n))

organs = ["Rectum", "Bladder"]
dose_time_let_volume_histograms = []

toxs = ["Grade2andabove", "Grade3.GU.CTCv3"]

nsteps = 1000
dosegrid = []
letgrid = []

for patient in cohort.list_of_patients:
    dosegrid = patient.rtdose_dose.pixel_array * patient.rtdose_dose.DoseGridScaling
    letgrid = patient.rtdose_letd.pixel_array * patient.rtdose_letd.DoseGridScaling

    dose_times_let = letgrid * dosegrid
    dose_times_let_in_organs = [dose_times_let[patient.structure_indices[patient.actual_structure_names[organ]]] for organ in organs]

    vhs_in_organs = [calculate_vh(arr, nsteps=nsteps) for arr in dose_times_let_in_organs]

    dose_time_let_volume_histograms.append(vhs_in_organs)

for i, organ in enumerate(organs):
    plt.figure()
    for j,prod_vhs in enumerate(dose_time_let_volume_histograms):

        patient = cohort.list_of_patients[j]
        tox = patient.metadata["Grade2andabove"]
        color="blue"
        if tox == 1:
            color = "red"

        x_array, this_organ_vh = prod_vhs[i]
        plt.plot(x_array, this_organ_vh, color=color, alpha=0.3)
    plt.xlabel("Dose x LET_D [Gy*kev/um]")
    plt.ylabel("Relative volume")
    plt.title("Dose times LET volume histogram for {}".format(organ))
    plt.savefig("dose_times_let_volume_histogram_{}.png".format(organ))




