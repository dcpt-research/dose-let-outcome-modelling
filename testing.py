import pydicom as pd
import numpy as np
import SimpleITK as sitk
import matplotlib.pyplot as plt
import os
from pyrt_lib_rasmusklitgaard import Patient
from pyrt_lib_rasmusklitgaard.helpers import find_struct_indices_in_rtdose

def structure_testing(patient_dir, organ_name):
    ds = pd.read_file(patient_dir+[d for d in os.listdir(patient_dir) if d[:8] =="RTSTRUCT"][0])
    p = Patient(patient_dir, "default")
    organ_name = p.actual_structure_names[organ_name]
    data = []
    
    index = -1
    for m in ds.StructureSetROISequence:
        if m.ROIName == organ_name:
            index = m.ROINumber
            break

    for m in ds.ROIContourSequence:
        if m.ReferencedROINumber == index:
            for n in m.ContourSequence:
                for point in n.ContourData:
                    data.append(point)

    data = np.reshape(data, (int(len(data)/3),3))

    slices = []
    temp = []
    z = None
    for dat in data:
        if dat[2] != z:
            slices.append(temp)
            temp = []
            z = dat[2]
        temp.append(dat)

    print(len(slices))
    slices = slices[1:]
    for slic in slices:
        z = slic[0][2]
        xs = [s[0] for s in slic]
        ys = [s[1] for s in slic]
        plt.figure()
        plt.plot(xs,ys,'r*')
        plt.title("z = {:7.1f}".format(z))
        plt.savefig("testing/test_z_{:7.1f}".format(z).replace(".","_")+".png")

def dose_masking_testing(patient_dir, organ_name):
    struct  = pd.read_file(patient_dir+[d for d in os.listdir(patient_dir) if d[:8] =="RTSTRUCT"][0])
    ds      = pd.read_file(patient_dir+[d for d in os.listdir(patient_dir) if d[:6] =="RTDOSE"][0])
    p = Patient(patient_dir, "default")
    organ_name = p.actual_structure_names[organ_name]


    structure_mask = p.structure_indices[organ_name]
    dosegrid = ds.pixel_array * ds.DoseGridScaling

    zeros = np.zeros_like(dosegrid)
    zeros[structure_mask] = 1
    organ_dose = zeros

    for i,slic in enumerate(organ_dose):
        plt.figure()
        plt.imshow(slic)
        plt.title("Slice {:04d}".format(i))
        plt.savefig("testing/organ_dose_{:04d}.png".format(i))


if __name__ == "__main__":
    patient_dir = "/mnt/d/simulation_data_dicom/198/"
    organ_name = "Bladder Wall"
    dose_masking_testing(patient_dir, organ_name)

    # structure_testing(patient_dir, organ_name)