import sklearn.preprocessing as skpp
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.inspection import permutation_importance
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
print("Imports completed")

datafile_name = "ufhpti_cohort_database.csv"
header = np.genfromtxt(datafile_name,delimiter=",",dtype=str)[0,:]
data = np.genfromtxt(datafile_name, delimiter=",",skip_header=1)
print("CSV data has been loaded")

outcome_columns = [3,4,5,6,7,8] # starts at index 3, ends at index 8
patient_id_column = [0]
skip_columns = patient_id_column + outcome_columns

x_header = [x for i,x in enumerate(header) if i not in skip_columns]
x_data = np.array([column for i,column in enumerate(data.T) if i not in skip_columns]).T

y_header = [y for i,y in enumerate(header) if i in outcome_columns]
y_data = np.array([column for i,column in enumerate(data.T) if i in outcome_columns]).T

x_data = skpp.normalize(x_data, norm="l2", axis=0) # axis 0 because features are rows
print("Data has been L2 normalized for each feature")

for i,y in enumerate(y_data.T):
    print("Splitting data into test and train for label {}".format(i))
    X_train, X_test, y_train, y_test = train_test_split(x_data, y, stratify=y)
    print("Training a random forest network for label {}".format(i))
    forest = RandomForestClassifier()
    forest.fit(X_train, y_train)
    print("Model has been trained for label {}".format(i))

    print("Evauluating perfomance and plotting features with labels.")
    importances = forest.feature_importances_
    std = np.std([tree.feature_importances_ for tree in forest.estimators_], axis=0)
    # forest_importances = pd.Series(importances, index=["{}".format(i) for i in range(len(x_header))])
    forest_importances = pd.Series(result.importances_mean, index=["{}".format(i) for i in range(len(x_header))])

    result = permutation_importance(
    forest, X_test, y_test, n_repeats=10, n_jobs=2
    )

    fig, ax = plt.subplots()
    forest_importances.plot.bar(yerr=result.importances_std/np.shape(X_train)[0], ax=ax)
    ax.set_title("Feature importances using MDI")
    ax.set_ylabel("Mean decrease in impurity")
    fig.tight_layout()
    plt.savefig("feature_importance/fig_{}.png".format(i))
    print("Figure saved for feature {}".format(i))
